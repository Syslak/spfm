#!/usr/bin/env python3

import os
import requests
from dotenv import load_dotenv
from datetime import datetime
from common import connect_to_spotify

def get_fm_tracks(users):
    tracks_dict = dict()
    period = "1month"
    length = 25
    for user in users:
        url = f"https://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user={user}&api_key={os.getenv('fm_key')}\
    &period={period}&format=json&limit={length}"
        res = requests.get(url).json()
        for track in res["toptracks"]["track"]:
            key = f"{track['artist']['name']} {track['name']}"
            if key in tracks_dict:
                if int(track["playcount"]) > tracks_dict[key]:
                    tracks_dict[key] = int(track["playcount"])
            else:
                tracks_dict[key] = int(track["playcount"])

    return dict(sorted(tracks_dict.items(), key=lambda item: item[1], reverse=True)).keys()


def add_tracks_to_playlist(tracks):
    sp = connect_to_spotify()

    track_uris = list()
    for i in tracks:
        res = sp.search(i)
        track_uris.append(res["tracks"]["items"][0]["uri"])
    sp.user_playlist_replace_tracks(os.getenv("user"),
                                    os.getenv("prelak_ID"),
                                    track_uris)


if __name__ == "__main__":
    load_dotenv()
    tracks = get_fm_tracks(["prvinspace", "syslak"])
    add_tracks_to_playlist(tracks)

    print(f"{datetime.now()}: Wrote songs to prelak\n")
