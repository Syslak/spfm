#!/usr/bin/env python3

import os
from dotenv import load_dotenv
from datetime import datetime
from common import connect_to_spotify


def write_top_tracks_to_playlist(playlist, time_range):
    sp = connect_to_spotify()
    top = sp.current_user_top_tracks(limit=50, time_range=time_range)
    uris = [i["uri"] for i in top["items"]]

    sp.user_playlist_replace_tracks(os.getenv("user"),
                                    playlist,
                                    uris)



if __name__ == "__main__":
    load_dotenv()
    write_top_tracks_to_playlist(os.getenv("tpa_ID"), "long_term")
    print(f"{datetime.now()}: Wrote songs to tpa\n")
    write_top_tracks_to_playlist(os.getenv("tp6_ID"), "medium_term")
    print(f"{datetime.now()}: Wrote songs to tpa\n")
