#!/usr/bin/env python3

import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
# from dotenv import load_dotenv

def connect_to_spotify():

    SCOPE = "playlist-modify-public user-top-read"

    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=os.getenv("client_ID"),
                                                   client_secret=os.getenv("client_secret"),
                                                   redirect_uri=os.getenv("callback_uri"),
                                                   scope=SCOPE,
                                                   cache_path=os.getenv("cache_path")))
    return sp
